.TH gfs2_mount 8

.SH NAME
gfs2_mount - GFS2 mount options

.SH SYNOPSIS
.B mount
[\fIStandardMountOptions\fR] \fB-t\fP gfs2 \fIDEVICE\fR \fIMOUNTPOINT\fR \fB-o\fP [GFS2Option1,GFS2Option2,GFS2OptionX...]

.SH DESCRIPTION
GFS2 may be used as a local (single computer) filesystem, but its real purpose
is in clusters, where multiple computers (nodes) share a common storage device.

Above is the format typically used to mount a GFS2 filesystem, using the
\fBmount\fP(8) command.  The \fIdevice\fR may be any block device on which you
have created a GFS2 filesystem.  Examples include a
single disk partition (e.g. /dev/sdb3), a loopback device, a device exported
from another node (e.g. an iSCSI device or a \fBgnbd\fP(8) device), or a
logical volume (typically comprised of a number of individual disks).

\fIdevice\fR does not necessarily need to match the device name as seen on
another node in the cluster, nor does it need to be a logical volume.  However,
the use of a cluster-aware volume manager such as CLVM2 (see \fBlvm\fP(8))
will guarantee that the managed devices are named identically on each node in a
cluster (for much easier management), and will allow you to configure a very
large volume from multiple storage units (e.g. disk drives).

\fIdevice\fR must make the entire filesystem storage area visible to the
computer.  That is, you cannot mount different parts of a single filesystem on
different computers.  Each computer must see an entire filesystem.  You
may, however, mount several GFS2 filesystems if you want to distribute your
data storage in a controllable way.

\fImountpoint\fR is the same as \fIdir\fR in the \fBmount\fP(8) man page.

This man page describes GFS2-specific options that can be passed to the GFS2 
file system at mount time, using the \fB-o\fP flag.  There are many other
\fB-o\fP options handled by the generic mount command \fBmount\fP(8).
However, the options described below are specifically for GFS2, and are not
interpreted by the mount command nor by the kernel's Virtual File System.  GFS2
and non-GFS2 options may be intermingled after the \fB-o\fP, separated by
commas (but no spaces).

As an alternative to mount command line options, you may send mount
options to gfs2 using "gfs2_tool margs" (after loading the gfs2 kernel
module, but before mounting GFS2).  For example, you may need to do
this when working from an initial ramdisk \fBinitrd\fP(4).  The
options are restricted to the ones described on this man page (no
general \fBmount\fP(8) options will be recognized), must not be
preceded by -o, and must be separated by commas (no spaces).  Example:

# gfs2_tool margs "lockproto=lock_nolock,ignore_local_fs"

Options loaded via "gfs2_tool margs" have a lifetime of only one GFS2
mount.  If you wish to mount another GFS2 filesystem, you must set
another group of options with "gfs2_tool margs".

The options debug, acl, quota, suiddir, and data can be
changed after mount using the "mount -o remount,option /mountpoint" command.
The options debug, acl, and suiddir support the "no"
prefix.  For example, "noacl" turns off what "acl" turns on.

If you have trouble mounting GFS2, check the syslog (e.g. /var/log/messages)
for specific error messages.

.SH OPTIONS
.TP
\fBlockproto=\fP\fILockModuleName\fR
This specifies which inter-node lock protocol is used by the GFS2 filesystem
for this mount, overriding the default lock protocol name stored in the
filesystem's on-disk superblock.

The \fILockModuleName\fR must be an exact match of the protocol name presented
by the lock module when it registers with the lock harness.  Traditionally,
this matches the .o filename of the lock module, e.g. \fIlock_dlm\fR,
or \fIlock_nolock\fR.

The default lock protocol name is written to disk initially when creating the
filesystem with \fBgfs2_mkfs\fP(8), -p option.  It can be changed on-disk by
using the \fBgfs2_tool\fP(8) utility's \fBsb proto\fP command.

The \fBlockproto\fP mount option should be used only under special
circumstances in which you want to temporarily use a different lock protocol
without changing the on-disk default.
.TP
\fBlocktable=\fP\fILockTableName\fR
This specifies the identity of the cluster and of the filesystem for this
mount, overriding the default cluster/filesystem identify stored in the
filesystem's on-disk superblock.  The cluster/filesystem name is recognized
globally throughout the cluster, and establishes a unique namespace for
the inter-node locking system, enabling the mounting of multiple GFS2
filesystems.

The format of \fILockTableName\fR is lock-module-specific.  For
lock_dlm, the format is \fIclustername:fsname\fR.  For
lock_nolock, the field is ignored.

The default cluster/filesystem name is written to disk initially when creating
the filesystem with \fBgfs2_mkfs\fP(8), -t option.  It can be changed on-disk
by using the \fBgfs2_tool\fP(8) utility's \fBsb table\fP command.

The \fBlocktable\fP mount option should be used only under special
circumstances in which you want to mount the filesystem in a different cluster,
or mount it as a different filesystem name, without changing the on-disk
default.
.TP
\fBlocalcaching\fP
This flag tells GFS2 that it is running as a local (not clustered) filesystem,
so it can turn on some block caching optimizations that can't be used when
running in cluster mode.

This is turned on automatically by the lock_nolock module,
but can be overridden by using the \fBignore_local_fs\fP option.
.TP
\fBlocalflocks\fP
This flag tells GFS2 that it is running as a local (not clustered) filesystem,
so it can allow the kernel VFS layer to do all flock and fcntl file locking.
When running in cluster mode, these file locks require inter-node locks,
and require the support of GFS2.  When running locally, better performance
is achieved by letting VFS handle the whole job.

This is turned on automatically by the lock_nolock module,
but can be overridden by using the \fBignore_local_fs\fP option.
.TP
\fBdebug\fP
Causes GFS2 to oops when encountering an error that would cause the
mount to withdraw or print an assertion warning.  This option should
probably not be used in a production system. 
.TP
\fBignore_local_fs\fP
By default, using the nolock lock module automatically turns on the
\fBlocalcaching\fP and \fBlocalflocks\fP optimizations.  \fBignore_local_fs\fP
forces GFS2 to treat the filesystem as if it were a multihost (clustered)
filesystem, with \fBlocalcaching\fP and \fBlocalflocks\fP optimizations
turned off.
.TP
\fBupgrade\fP
This flag tells GFS2 to upgrade the filesystem's on-disk format to the version
supported by the current GFS2 software installation on this computer.
If you try to mount an old-version disk image, GFS2 will notify you via a syslog
message that you need to upgrade.  Try mounting again, using the
\fB-o upgrade\fP option.  When upgrading, only one node may mount the GFS2
filesystem.
.TP
\fBnum_glockd=\fP\fINumber\fR
Tunes GFS2 to alleviate memory pressure when rapidly acquiring many locks (e.g.
several processes scanning through huge directory trees).  GFS2' glockd kernel
daemon cleans up memory for no-longer-needed glocks.  Multiple instances
of the daemon clean up faster than a single instance.  The default value is
one daemon, with a maximum of 16.  Since this option was introduced, other
methods of rapid cleanup have been developed within GFS2, so this option may go
away in the future.
.TP
\fBacl\fP
Enables POSIX Access Control List \fBacl\fP(5) support within GFS2.
.TP
\fBspectator\fP
Mount this filesystem using a special form of read-only mount.  The mount
does not use one of the filesystem's journals.
.TP
\fBsuiddir\fP
Sets owner of any newly created file or directory to be that of parent
directory, if parent directory has S_ISUID permission attribute bit set.
Sets S_ISUID in any new directory, if its parent directory's S_ISUID is set.
Strips all execution bits on a new file, if parent directory owner is different
from owner of process creating the file.  Set this option only if you know
why you are setting it.
.TP
\fBquota=\fP\fI[off/account/on]\fR
Turns quotas on or off for a filesystem.  Setting the quotas to be in
the "account" state causes the per UID/GID usage statistics to be
correctly maintained by the filesystem, limit and warn values are
ignored.  The default value is "off".
.TP
\fBdata=\fP\fI[ordered/writeback]\fR
When data=ordered is set, the user data modified by a transaction is
flushed to the disk before the transaction is committed to disk.  This
should prevent the user from seeing uninitialized blocks in a file
after a crash.  Data=writeback mode writes the user data to the disk
at any time after it's dirtied.  This doesn't provide the same
consistency guarantee as ordered mode, but it should be slightly
faster for some workloads.  The default is ordered mode.

.SH LINKS
.TP 30
http://sources.redhat.com/cluster
-- home site of GFS2
.TP
http://www.suse.de/~agruen/acl/linux-acls/
-- good writeup on ACL support in Linux

.SH SEE ALSO

\fBgfs2\fP(8), 
\fBmount\fP(8) for general mount options,
\fBchmod\fP(1) and \fBchmod\fP(2) for access permission flags,
\fBacl\fP(5) for access control lists,
\fBlvm\fP(8) for volume management,
\fBccs\fP(7) for cluster management,
\fBumount\fP(8),
\fBinitrd\fP(4).

