#ifndef __FS_RECOVERY_H__
#define __FS_RECOVERY_H__

#include "libgfs2.h"

int replay_journals(struct gfs2_sbd *sdp);

#endif /* __FS_RECOVERY_H__ */

