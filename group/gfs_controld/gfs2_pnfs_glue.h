/*
 *  group/libgfscontrol/gfs2_pnfs_glue.h
 *
 *  Copyright (c) 2008 The Regents of the University of Michigan.
 *  All rights reserved.
 *
 *  David M. Richter <richterd@citi.umich.edu>
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *  1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *  3. Neither the name of the University nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 *  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 *  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  With thanks to CITI's project sponsor and partner, IBM.
 */

#ifndef _GFS2_PNFS_GLUE_H_
#define _GFS2_PNFS_GLUE_H_

/*
 * This file is meant to provide the GFS2 userland with enough information
 * about the pNFS parts.
 */

/*
 * group/libgfscontrol/pnfs_gfs2.h
 *
 * This is for the gfs_controld<->gfs_controld multicast channel.
 * See cpg-new.c for the "normal" GFS_MSG_*; we just pick a large value.
 */
#define GFS_MSG_PNFS	50000

/*
 * group/libgfscontrol/pnfs_gfs2.h
 *
 * This is for the gfs_pnfs_control<->gfs_controld channel.
 * See gfs_controld.h for the "normal" GFSC_CMD_*; we just pick a large value.
 */
#define GFSC_CMD_PNFS	50000


/* group/gfs_controld/gfs_daemon.h */
struct mountgroup;

/* openAIS */
struct cpg_name;

/* group/gfs_controld/cpg-new.c */
struct gfs_header;

/* group/gfs_controld/pnfs_gfs2.c */
extern int setup_pnfs(void);
extern void shutdown_pnfs(void);
extern void pnfs_do_mount_done(struct mountgroup *, int);
extern void process_pnfs_upcall(int);
extern void process_pnfs_multicast(struct cpg_name *, struct mountgroup *,
				   struct gfs_header *, int);
#endif /* _GFS2_PNFS_GLUE_H_ */
