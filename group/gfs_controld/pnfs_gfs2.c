/*
 *  group/gfs_controld/pnfs_gfs2.c
 *
 *  Copyright (c) 2008 The Regents of the University of Michigan.
 *  All rights reserved.
 *
 *  David M. Richter <richterd@citi.umich.edu>
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *  1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *  3. Neither the name of the University nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 *  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 *  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  With thanks to CITI's project sponsor and partner, IBM.
 */

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include "pnfs_gfs2.h"


static int pnfs_pipe_fd = -2;
static int local_msgid;
static LIST_HEAD(pending_pnfs_requests);

static u32 mds_nodeid;
static struct gfs2_ds_list *pnfs_ds_list;

#define i_am_the_mds() (mds_nodeid == our_nodeid)

/*
 * Represents a queued request where the query downcall has been
 * made and the response upcall is pending.
 */
struct pnfs_request {
	u32 local_msgid;
	u32 orig_msgid;
	u32 groupid;
	u32 reply_to_nodeid;
	struct list_head list;
};

/*
 * When a request is received over multicast, save enough info about the
 * sender so that, once we've passed along the request in a downcall and
 * have received the reply upcall from the kernel, we can reply to the
 * appropriate cluster node.
 */
static struct pnfs_request* alloc_init_request(u32 msgid, u32 groupid,
					       u32 reply_to_nodeid,
					       struct list_head *waitlist)
{
	struct pnfs_request *request;

	request = malloc(sizeof(*request));
	if (!request)
		return NULL;

	request->local_msgid = local_msgid++;
	request->orig_msgid = msgid;
	request->groupid = groupid;
	request->reply_to_nodeid = reply_to_nodeid;
	INIT_LIST_HEAD(&request->list);
	list_add(&request->list, waitlist);

	return request;
}

static void free_request(struct pnfs_request *request)
{
	list_del(&request->list);
	free(request);
}

/*
 * When a reply upcall is received, look up the requester's information so we
 * can multicast the reply to them.
 */
static struct pnfs_request* find_request_msgid(u32 msgid, struct list_head *list)
{
	struct pnfs_request *request;

	list_for_each_entry(request, list, list)
		if (request->local_msgid == msgid)
			return request;
	return NULL;
}

/*
 * Open the rpc_pipefs pipe that lets us make the up/downcalls that pNFS uses
 * as part of its control channel.
 */
int setup_pnfs(void)
{
	/*
	 * XXX: if we can't open the pipe.. fail with message.
	 * - UNLESS they gave qc like --ignore-pnfs-failure
	 */
	pnfs_pipe_fd = open(PNFS_PIPE_PATH, O_RDWR, 0);
	if (pnfs_pipe_fd < 0)
		log_error("%s: can't open pnfs pipe '%s' (%d, errno %d)"
			  ".\n", __func__, PNFS_PIPE_PATH, pnfs_pipe_fd, errno);

	return pnfs_pipe_fd;
}

void shutdown_pnfs(void)
{
	/* XXX: print warnings about any pending requests that are going to
	 * get squelched.  or, maybe more importantly, reply with errors
	 * right away.
	 */
	close(pnfs_pipe_fd);
}

#define KEY_NTH_NODE	"/cluster/clusternodes/clusternode[%u]/%s"
#define KEY_NODEID_PNFS	"/cluster/clusternodes/clusternode[@nodeid=\"%u\"]/pnfs/%s"

#define FIELD_NAME	"@name"
#define FIELD_NODEID	"@nodeid"
#define FIELD_IS_MDS	"@is_mds"
#define FIELD_DS_IP	"@ds_ip"

#define CCS_PATH_MAX	256
#define MAX_DSES_GUESS	100
#define MAX_IPV4_STRLEN	15

/*
 * Using libccs, look in cluster.conf for @get_field within a nested set of
 * tags specified as an xpath by @base_path.
 *
 * In this case, @base_path is expected to have an integer-type matching
 * template in the format.
 */
char* value_for_int_key(const char *base_path, int key, const char *get_field)
{
	int err;
	char *val, path[CCS_PATH_MAX];

	err = snprintf(path, CCS_PATH_MAX, base_path, key, get_field);
	if (err < 0) {
		log_error("%s: couldn't do it for base=|%s| key=|%d| get_field="
			  "|%s|\n", __func__, base_path, key, get_field);
		return NULL;
	}

	err = ccs_get(ccs_handle, path, &val);
	if (err)
		return NULL;

	if (!err && val == NULL)
		log_error("%s: WARNING: ccs_get() returned success, but no "
			  "value for field |%s|\n", __func__, get_field);
	return val;
}

/*
 * Look for pNFS setup info in cluster.conf.  Specifically, look for a single
 * node to be labelled as the MDS (<pnfs is_mds="1"/>), and one or more nodes
 * to be labelled as DSes with their IP address (<pnfs ds_ip="10.1.1.10"/>).
 *
 * XXX: we could also specify a default stripe width, e.g., in the future.
 */
int parse_cluster_conf(void)
{
	int i, nodeid, num_dses = 0, err = 0;
	int size, ds_list_len = MAX_DSES_GUESS;
	char *val, *val2;
	struct gfs2_ds_info **dses, *ds;

	dses = calloc(ds_list_len, sizeof(ds));
	if (!dses) {
		log_error("%s: ERROR: unable to alloc ds_list (len %d)\n",
			  __func__, ds_list_len);
		err = 1;
		goto out;
	}

	for (i = 1; ; i++) {
		val = value_for_int_key(KEY_NTH_NODE, i, FIELD_NODEID);
		if (!val)
			break;

		nodeid = atoi(val);
		if (nodeid == 0) {
			log_error("%s: ERROR: 0 can't be a nodeid\n", __func__);
			err = 1;
			goto out;
		}

		val2 = value_for_int_key(KEY_NODEID_PNFS, nodeid, FIELD_IS_MDS);
		if (!val2 || !(*val2 == '1' || tolower(*val2) == 'y' ||
			       tolower(*val2) == 't'))
			goto check_ds_ip;

		if (mds_nodeid != 0)
			log_error("%s: ERROR: MDS nodeid already %d; IGNORING "
				  "extra val %s\n", __func__, mds_nodeid, val2);
		else
			mds_nodeid = nodeid;
		free(val2);

	check_ds_ip:
		val2 = value_for_int_key(KEY_NODEID_PNFS, nodeid, FIELD_DS_IP);
		if (!val2)
			continue;
		if (strlen(val2) > MAX_IPV4_STRLEN) {
			log_error("%s: ERROR: DS IP address |%s| is too long "
				  "for an IPv4 dotted-quad\n", __func__, val2);
			err = 1;
			break;
		}

		ds = calloc(1, sizeof(*ds));
		if (!ds) {
			log_error("%s: ERROR: unable to alloc %dth DS (nodeid"
				  " %d)\n", __func__, num_dses + 1, nodeid);
			err = 1;
			break;
		}

		ds->nodeid = nodeid;
		strcpy(ds->ip_quad, val2);
		dses[num_dses++] = ds;
		if (num_dses == ds_list_len) {
			ds_list_len *= 2;
			dses = realloc(dses, sizeof(ds) * ds_list_len);
			if (!dses) {
				log_error("%s: ERROR: unable to realloc ds_list"
					  "(len %d)\n", __func__, ds_list_len);
				err = 1;
				break;
			}
		}
		free(val2);
		free(val);
	}

	size = sizeof(*pnfs_ds_list) + (sizeof(*ds) * num_dses);
	pnfs_ds_list = calloc(1, size);
	if (!pnfs_ds_list) {
		log_error("%s: ERROR: unable to alloc pnfs_ds_list (size %d)\n",
			  __func__, size);
		err = 1;
		goto out;
	}

	pnfs_ds_list->num_dses = num_dses;
	for (i = 0; i < num_dses; i++)
		memcpy(&pnfs_ds_list->dses[i], dses[i], sizeof(*ds));
out:
	return err;
}


/*
 * Make a downcall to the in-kernel GFS code on the rpc_pipefs pipe.
 */
static int pnfs_downcall(struct pipefs_hdr *msg)
{
	int i;

	i = atomicio(write, pnfs_pipe_fd, msg, msg->totallen);
	if (i != msg->totallen) {
		log_error("%s: ERROR: short write (%d vs %d)\n", __func__,
			  msg->totallen, i);
		return -EIO;
	}
	log_error("%s: DOWNCALL: sent type %d, status %d, len %d\n", __func__,
		  msg->type, msg->status, msg->totallen);

	return 0;
}

/*
 * Store our_nodeid, mds_nodeid, and the mcast groupid in the GFS superblock.
 * PNFS_GFS2_SET_NODEID uses no msgid.
 */
static int pnfs_do_set_nodeid(u32 groupid)
{
	int totallen, err = ENOMEM;
	struct pipefs_hdr *msg = NULL;
	struct gfs2_set_nodeid *gsn;

	totallen = sizeof(*msg) + sizeof(*gsn);
	msg = calloc(1, totallen);
	if (!msg) {
		log_error("%s: ERROR: couldn't alloc %d\n", __func__, totallen);
		goto out;
	}
	msg->type = PNFS_GFS2_SET_NODEID;
	msg->totallen = totallen;
	gsn = payload_of(msg);
	gsn->our_nodeid = our_nodeid;
	gsn->mds_nodeid = mds_nodeid;
	gsn->groupid = groupid;

	err = pnfs_downcall(msg);
	if (err)
		log_error("%s: ERROR: couldn't pnfs_downcall for SET NODEID "
			  "(%d)\n", __func__, err);
out:
	if (msg)
		free(msg);
	return err;
}

/*
 * Have the MDS store its list of PNFS Data Servers.
 * PNFS_GFS2_SET_DS_LIST uses no msgid.
 */
static int pnfs_do_set_ds_list()
{
	int llen, totallen, err = ENOMEM;
	struct pipefs_hdr *msg = NULL;

	llen = sizeof(*pnfs_ds_list) +
		(pnfs_ds_list->num_dses * sizeof(struct gfs2_ds_info));
	totallen = sizeof(*msg) + llen;
	msg = calloc(1, totallen);
	if (!msg) {
		log_error("%s: ERROR: couldn't alloc %d\n", __func__, totallen);
		goto out;
	}
	msg->type = PNFS_GFS2_SET_DS_LIST;
	msg->totallen = totallen;
	memcpy(payload_of(msg), pnfs_ds_list, llen);

	err = pnfs_downcall(msg);
	if (err)
		log_error("%s: ERROR: couldn't pnfs_downcall for SET DS_LIST "
			  "(%d)\n", __func__, err);
out:
	if (msg)
		free(msg);
	return err;
}

/*
 * Once we've mounted, we need to record some nodeid information for later
 * multicasting.  On the MDS, we also record the DS list.
 */
void pnfs_do_mount_done(struct mountgroup *mg, int mount_result)
{
	int err;

	if (mount_result) {
		log_error("%s: ERROR: mount_result is %d -- not making pNFS "
			  "downcall\n", __func__, mount_result);

		if (mount_result != mg->kernel_mount_error)
			log_error("%s: WARNING: mount_result (%d) doesn't "
				  "match mg->kernel_mount_error (%d)\n",
				  __func__, mount_result,
				  mg->kernel_mount_error);
		return;
	}

	/* get MDS nodeid and DS list */
	if (parse_cluster_conf())
		log_error("%s: an error occurred while trying to extract pNFS"
			  "-related info from cluster.conf.  pNFS may not "
			  "be in working order.\n", __func__);
	if (mds_nodeid == 0) {
		/* XXX */
		log_error("%s: WARNING: MDS nodeid is unset! Defaulting to "
			  "mds_nodeid=1\n", __func__);
		mds_nodeid = 1;
	}

	/* set our/mds nodeid and mcast groupid */
	err = pnfs_do_set_nodeid(mg->id);
	if (err) {
		log_error("%s: ERROR: couldn't set PNFS ids. err %d\n",
			  __func__, err);
		return;
	}

	/* if we're the MDS, record the DS list */
	if (i_am_the_mds()) {
		if (!pnfs_ds_list) {
			log_error("%s: ERROR: no DS nodes found!\n", __func__);
			return;
		}
		err = pnfs_do_set_ds_list();
		if (err)
			log_error("%s: ERROR: couldn't set PNFS ds_list. err %d"
				  "\n", __func__, err);
	}
}


#define swap16(num, in)	(in ? le16_to_cpu(num) : cpu_to_le16(num))
#define swap32(num, in)	(in ? le32_to_cpu(num) : cpu_to_le32(num))
#define swap64(num, in)	(in ? le64_to_cpu(num) : cpu_to_le64(num))
#define HOST_TO_NET  0
#define NET_TO_HOST  1

static void marshall_get_state(struct pipefs_hdr *msg,  int in)
{
	struct gfs2_pnfs_get_state *ggs = payload_of(msg);
	struct pnfs_get_state *gs = &ggs->pnfs_get_state;
	struct nfs4_generic_stateid *stid = &gs->stid;
	struct nfs4_stateid_common *stidc = &stid->gs_common;
	clientid_t *clid = &gs->clid;

	ggs->groupid = swap32(ggs->groupid, in);
	gs->dsid = swap32(gs->dsid, in);
	gs->ino = swap64(gs->ino, in);
	stidc->si_generation = swap32(stidc->si_generation, in);
	stidc->si_boot = swap32(stidc->si_boot, in);
	stid->gs_other[0] = swap32(stid->gs_other[0], in);
	stid->gs_other[1] = swap32(stid->gs_other[1], in);
	clid->cl_boot = swap32(clid->cl_boot, in);
	clid->cl_id = swap32(clid->cl_id, in);
	gs->access = swap32(gs->access, in);
	gs->stid_gen = swap32(gs->stid_gen, in);
	gs->verifier[0] = swap32(gs->verifier[0], in);
	gs->verifier[1] = swap32(gs->verifier[1], in);
}

static void marshall_get_verifier(struct pipefs_hdr *msg, int in)
{
	struct gfs2_pnfs_get_verifier *ggv = payload_of(msg);

	ggv->groupid = swap32(ggv->groupid, in);
}

static void marshall_inout(struct pipefs_hdr *msg, int in)
{
	msg->msgid = swap32(msg->msgid, in);
	msg->totallen = swap16(msg->totallen, in);
	msg->status = swap32(msg->status, in);

	if (msg->type == PNFS_GET_STATE_REQ ||
	    msg->type == PNFS_GET_STATE_RES)
		marshall_get_state(msg, in);
	else if (msg->type == PNFS_GET_VERIFIER_REQ ||
		 msg->type == PNFS_GET_VERIFIER_RES)
		marshall_get_verifier(msg, in);
}

/*
 * Multicasts a copy of @msg to @to_nodeid within the cpg @groupid.
 */
static int pnfs_multicast(struct pipefs_hdr *msg, u32 to_nodeid, u32 groupid)
{
	int full_size, err;
	struct mountgroup *mg;
	struct gfs_header *hd;

	mg = find_mg_id(groupid);
	if (!mg) {
		log_error("%s: ERROR: no mountgroup for id %d\n", __func__,
			  groupid);
		err = EINVAL;
		goto out;
	}

	full_size = sizeof(*hd) + msg->totallen;
	hd = calloc(1, full_size);
	if (!hd) {
		err = ENOMEM;
		goto out;
	}
	hd->type = GFS_MSG_PNFS;
	hd->to_nodeid = to_nodeid;
	marshall_inout(msg, HOST_TO_NET);
	memcpy(payload_of(hd), msg, msg->totallen);

	log_error("%s: node %u MCASTING to node %u (groupdid %u)\n", __func__,
		  our_nodeid, hd->to_nodeid, groupid);
	gfs_send_message(mg, (char *)hd, full_size);
	err = 0;
out:
	return err;
}

static struct pipefs_hdr* pnfs_upcall_readmsg(void)
{
	int i, hsize, left;
	struct pipefs_hdr hdr, *msg = NULL;

	hsize = sizeof(hdr);
	memset(&hdr, 0, hsize);
	i = atomicio(read, pnfs_pipe_fd, &hdr, hsize);
	if (i != hsize) {
		log_error("%s: EIO: wanted to read %d, got %d\n", __func__,
			  hsize, i);
		goto out;
	}

	msg = calloc(1, hdr.totallen);
	if (!msg)
		goto out;

	memcpy(msg, &hdr, hsize);
	left = hdr.totallen - hsize;
	i = atomicio(read, pnfs_pipe_fd, payload_of(msg), left);
	if (i != left) {
		log_error("%s: EIO: wanted %d, got %d. msgid=%d\n", __func__,
			  left, i, hdr.msgid);
		free(msg);
		msg = NULL;
	}
out:
	return msg;
}

/*
 * Handle an incoming pnfs multicast message.
 * OpenAIS frees the message later.
 */
void process_pnfs_multicast(struct cpg_name *group_name, struct mountgroup *mg,
			    struct gfs_header *hd, int len)
{
	struct pipefs_hdr *msg;
	struct pnfs_request *request;

	msg = payload_of(hd);
	marshall_inout(msg, NET_TO_HOST);

	log_error("%s: GOT MCAST group_name='%s' from_nodeid=%d to_nodeid=%d "
		  "msgid=%d type=%d status=%d\n", __func__, group_name->value,
		  hd->nodeid,hd->to_nodeid, msg->msgid, msg->type, msg->status);

	if (hd->to_nodeid != CPG_MCAST_ALL && hd->to_nodeid != our_nodeid)
		goto out;

	if (IS_REQ(msg->type)) {
		request = alloc_init_request(msg->msgid, mg->id, hd->nodeid,
					     &pending_pnfs_requests);
		if (!request) {
			log_error("%s: unable to alloc pnfs_request "
				  "for msgid %d\n", __func__, msg->msgid);
			goto out;
		}

		/* temporarily replace the msgid to avoid collisions */
		msg->msgid = request->local_msgid;
	}

	if (pnfs_downcall(msg)) {
		log_error("%s: ERROR doing downcall (for msgid=%d type=%d "
			  "status=%d)\n", __func__, msg->msgid, msg->type,
			  msg->status);
	}
out:
	return;
}

/*
 * Handle an upcall from gfs-kernel that's waiting on the rpc_pipefs pipe.
 */
void process_pnfs_upcall(int ci)
{
	int err = 0, to_nodeid, groupid;
	struct pipefs_hdr *msg;
	struct pnfs_request *req = NULL;
	struct gfs2_pnfs_get_state *ggs;
	struct gfs2_pnfs_get_verifier *ggv;

	msg = pnfs_upcall_readmsg();
	if (!msg) {
		log_error("%s: unable to process upcall.\n", __func__);
		goto out;
	}

	log_error("%s: UPCALL: msgid %d, type %d, status %d, totallen %d\n",
		  __func__, msg->msgid, msg->type, msg->status, msg->totallen);

	if (IS_RES(msg->type)) {
		req = find_request_msgid(msg->msgid, &pending_pnfs_requests);
		if (!req) {
			log_error("%s: ERROR: can't find req for msgid %u type"
				  " %d\n", __func__, msg->msgid, msg->type);
			goto out;
		}

		/* restore the original msgid */
		msg->msgid = req->orig_msgid;
		groupid = req->groupid;
		to_nodeid = req->reply_to_nodeid;
	} else if (msg->type == PNFS_GET_STATE_REQ) {
		ggs = payload_of(msg);
		ggs->pnfs_get_state.dsid = our_nodeid;
		groupid = ggs->groupid;
		to_nodeid = mds_nodeid;
	} else if (msg->type == PNFS_CHANGE_STATE_NOTIF) {
		/* XXX: what if the MDS is also a DS?
		 *  A)  Perhaps we could just handle it all in-kernel, a la
		 *      get_verifier()?  So how does the MDS know if it's also
		 *      a DS?
		 */
		ggs = payload_of(msg);
		groupid = ggs->groupid;
		to_nodeid = CPG_MCAST_ALL;
	} else if (msg->type == PNFS_GET_VERIFIER_REQ) {
		ggv = payload_of(msg);
		groupid = ggv->groupid;
		to_nodeid = mds_nodeid;
	} else {
		log_error("%s: ERROR bad msg type %d\n", __func__, msg->type);
		goto out;
	}

	err = pnfs_multicast(msg, to_nodeid, groupid);
out:
	if (msg)
		free(msg);
	if (req)
		free_request(req);
	return;
}


