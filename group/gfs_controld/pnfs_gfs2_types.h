/*
 *  group/libgfscontrol/pnfs_gfs2_types.h
 *
 *  Copyright (c) 2008 The Regents of the University of Michigan.
 *  All rights reserved.
 *
 *  David M. Richter <richterd@citi.umich.edu>
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *  1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *  3. Neither the name of the University nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 *  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 *  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  With thanks to CITI's project sponsor and partner, IBM.
 */

#ifndef _PNFS_GFS2_TYPES_H_
#define _PNFS_GFS2_TYPES_H_

/*
 * This file is meant to be used by userland gfs2 to be compatible
 * with the gfs2/pnfs kernel.
 *
 * A similar file of the same name exists in-kernel; however, it lacks
 * the various nfs-specific types defined herein.  We include them here
 * to avoid having to compile against kernel headers.
 */

#include <stdlib.h>
#include <stddef.h>
#include "list.h"

typedef u_int64_t u64;
typedef u_int32_t u32;
typedef u_int16_t u16;
typedef u_int8_t  u8;


/*
 * Message header format used in pNFS-related GFS2 messages.  When sent over
 * one of the existing GFS channels we use -- (which are:
 *   (a) the unix-socket-based gfs_control<->gfs_controld channel, and
 *   (b) the multicast-based gfs_controld<->gfs_controld channel.
 * ) -- struct gfs2_pnfs_messages are wrapped by the appropriate GFS header.
 *
 * A new, third GFS-related channel is introduced for pNFS and uses basically
 * the same messages without an initial GFS-specific header:
 *   (c) the rpc_pipefs-based gfs_controld<->gfs-kernel channel
 *
 * Note that we use (c) above instead of GFS's existing kernel<->userland
 * communication methods, which are too limited for our purposes (upcalls are
 * made by broadcasting uevents from the kernel over a netlink socket;
 * "downcalls" aren't really downcalls -- it's just individual sysfs variables
 * getting twiddled).
 *
 * As a message example, commands on channel (a) above use a struct gfsc_header,
 * so a "get_state" request from gfs_pnfs_control in userland would be sent to
 * its local gfs_controld and look something like:
 *
 * -----------------------------
 * | struct gfsc_header        | .option = GFSC_CMD_PNFS  (see gfs_controld.h)
 * |---------------------------|
 * || struct pipefs_hdr       || .type = PNFS_GET_STATE_REQ
 * ||-------------------------||
 * ||| struct pnfs_get_state |||
 * |||-----------------------|||
 * -----------------------------
 *
 * For channel (b) above, the format uses a struct gfs_header (that is, not
 * struct gfsc_header like (a) uses) when sending a "get_state" request:
 *
 * -----------------------------
 * | struct gfs_header         | .type = GFS_MSG_PNFS  (XXX: see cpg-new.c)
 * |---------------------------|
 * || struct pipefs_hdr       || .type = PNFS_GET_STATE_REQ
 * ||-------------------------||
 * ||| struct pnfs_get_state |||
 * |||-----------------------|||
 * -----------------------------
 *
 * NB: MUST be kept in-sync with the kernel!
 */

#define IS_REQ(type)	(type == PNFS_GET_STATE_REQ || \
			 type == PNFS_GET_VERIFIER_REQ)
#define IS_RES(type)	(type == PNFS_GET_STATE_RES || \
			 type == PNFS_GET_VERIFIER_RES)
enum {
	PNFS_INVAL = 0,
	PNFS_GFS2_SET_NODEID = 1,
	PNFS_GFS2_SET_DS_LIST = 2,
	PNFS_GET_STATE_REQ = 3,
	PNFS_GET_STATE_RES = 4,
	PNFS_CHANGE_STATE_NOTIF = 5,
	PNFS_GET_VERIFIER_REQ = 6,
	PNFS_GET_VERIFIER_RES = 7,
};

/*
 * The NFS/pNFS data structures passed along the control channel.
 * These must be kept in-sync with their counterparts in the NFS headers.
 */

#define payload_of(headerp)  ((void *)(headerp + 1))

/* XXX: struct pipefs_hdr: 	include/linux/sunrpc/simple_rpc_pipefs.h */
struct pipefs_hdr {
	u32 msgid;
	u8  type;
	u8  flags;
	u16 totallen; // length of entire message, including hdr itself
	u32 status;
};


/* XXX: clientid_t:		include/linux/nfsd/state.h */
typedef struct {
	u32             cl_boot;
	u32             cl_id;
} clientid_t;


/* XXX: nfs4_stateid_xdr:	include/linux/nfsd/state.h */
/* nfs4 stateid as defined by the protocol */
struct nfs4_stateid_xdr {
	u32			si_generation;
	u32			si_opaque[3];
} __attribute__((packed));

/* XXX: nfs4_stateid_common:	include/linux/nfsd/state.h */
/* common to all stateid types */
struct nfs4_stateid_common {
	u32                     si_generation;
	u32                     si_boot;
} __attribute__((packed));

/* XXX: nfs4_generic_stateid:	include/linux/nfsd/state.h */
struct nfs4_generic_stateid {
	struct nfs4_stateid_common	gs_common;
	u32				gs_other[2];
} __attribute__((packed));

#define STATEID_FMT	"(%08x/%08x/%08x/%08x)"
#define STATEID_VAL(s) \
	((struct nfs4_stateid_xdr *)(s))->si_opaque[0], \
	((struct nfs4_stateid_xdr *)(s))->si_opaque[1], \
	((struct nfs4_stateid_xdr *)(s))->si_opaque[2], \
	((struct nfs4_stateid_xdr *)(s))->si_generation


/* XXX: struct pnfs_get_state:	include/linux/nfsd/pnfsd.h */
struct pnfs_get_state {
	u32			dsid;    /* request */
	u64			ino;      /* request */
	struct nfs4_generic_stateid stid;     /* request;response */
	clientid_t		clid;     /* response */
	u32			access;    /* response */
	u32			stid_gen;    /* response */
	u32			verifier[2]; /* response */
};

/* XXX: nfs4_verifier:		include/linux/nfs4.h */
#define NFS4_VERIFIER_SIZE	8
typedef struct { char data[NFS4_VERIFIER_SIZE]; } nfs4_verifier;


/* XXX: gfs2_set_nodeid:	fs/gfs2/pnfs_gfs2_types.h */
struct gfs2_set_nodeid {
	u32 			our_nodeid;
	u32			mds_nodeid;
	u32 			groupid;
};

/* XXX: gfs2_ds_info:		fs/gfs2/pnfs_gfs2_types.h */
struct gfs2_ds_info {
	u32 nodeid;
	char ip_quad[16];	/* up to 12 digits + 3 decimals + NULL-term */
};

/* XXX: gfs2_ds_list:		fs/gfs2/pnfs_gfs2_types.h */
struct gfs2_ds_list {
	int num_dses;
	struct gfs2_ds_info dses[0];
};

/* XXX: gfs2_pnfs_get_state:	fs/gfs2/pnfs_gfs2_types.h */
struct gfs2_pnfs_get_state {
	struct pnfs_get_state	pnfs_get_state;
	u32 			groupid;
};

/* XXX: gfs2_pnfs_get_verifier: fs/gfs2/pnfs_gfs2_types.h */
struct gfs2_pnfs_get_verifier {
	nfs4_verifier 		verifier;
	u32			groupid;
};

#endif /* _PNFS_GFS2_TYPES_H_ */
