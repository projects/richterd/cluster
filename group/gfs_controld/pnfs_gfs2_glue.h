/*
 *  group/libgfscontrol/pnfs_gfs2_glue.h
 *
 *  Copyright (c) 2008 The Regents of the University of Michigan.
 *  All rights reserved.
 *
 *  David M. Richter <richterd@citi.umich.edu>
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *  1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *  3. Neither the name of the University nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 *  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 *  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  With thanks to CITI's project sponsor and partner, IBM.
 */

#ifndef _PNFS_GFS2_GLUE_H_
#define _PNFS_GFS2_GLUE_H_

/*
 * This file is meant to provide the pNFS parts with enough information
 * about the rest of the GFS2 userland.
 */

#include <stdio.h>
#include <syslog.h>
#include <time.h>
#include <sys/types.h>
#include <corosync/cpg.h>

/* order matters here w/r/t libgfscontrol.h */
typedef u_int64_t uint64_t;
typedef u_int32_t uint32_t;
typedef u_int16_t uint16_t;

#include "libgfscontrol.h"
#include "gfs_controld.h"
#include "gfs_daemon.h"
#include "config.h"
#include "ccs.h"

/* a multicast message addressed to nodeid 0 means all nodes process it */
#define CPG_MCAST_ALL	0

/* group/gfs_controld/cpg-new.c */
struct gfs_header {
	uint16_t version[3];
	uint16_t type;          /* GFS_MSG_ */
	uint32_t nodeid;        /* sender */
	uint32_t to_nodeid;     /* recipient, 0 for all */
	uint32_t global_id;     /* global unique id for this lockspace */
	uint32_t flags;         /* GFS_MFLG_ */
	uint32_t msgdata;       /* in-header payload depends on MSG type */
	uint32_t pad1;
	uint64_t pad2;
};

/* group/gfs_controld/cpg-new.c */
extern void gfs_send_message(struct mountgroup *, char *, int);

/* group/gfs_controld/main.c */
extern void do_reply(int, int, char *, int, void *, int);

/* group/gfs_controld/config.c */
extern int ccs_handle;

#endif /* _PNFS_GFS2_GLUE_H_ */
